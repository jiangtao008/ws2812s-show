#ifndef RGBLED_H
#define RGBLED_H

#include <QWidget>
#include <QPainter>

enum LedShape
{
    circular,
    square,
};

class RGBLed : public QWidget
{
    Q_OBJECT
public:
    explicit RGBLed(QWidget *parent = nullptr);

    void setShape(LedShape shape);
    void setShapeToCir();       //设置灯为圆状
    void setShapeToSqu();       //设置灯为方状

    void setColor(QColor color);    //设置led颜色
    void setColor(unsigned char red, unsigned char green, unsigned char blue);
    QColor getLedColor();   //获取led颜色

    //设置led灯大小，此处为绝对大小，即限制了最大最小，直接固定在一个一个大小
    void setFixedSize(int width, int hight); //设置led大小
    void unFixedSize(int miniWidth = 0,int miniHight = 0);  //取消固定大小，一般不建议使用。

    //led有两种状态，一种是点亮状态，正常显示设置的颜色，第二种是未点亮（熄灭）的状态,熄灭状态的颜色也可以设置，一般为黑灰色。
    bool isLight();     //判断是否点亮
    void setLight(bool light = true);  //设置是否点亮
    void setUnlightColor(QColor color = QColor(50,50,50));  //设置未点亮状态下颜色

    //hsv格式转rgb格式，静态模式，外界可直接使用
    static void HSVtoRGB(unsigned char *r, unsigned char *g, unsigned char *b, int h, int s, int v);
signals:

private:
    void paintEvent(QPaintEvent *paintEvent) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    QColor          mColor;         //led灯颜色
    LedShape        mShape;         //led灯形状
    bool            lightFlag;      //灯是否点亮
    QColor          unlightColor;   //熄灭状态的颜色，默认(50,50,50)

};

#endif // RGBLED_H
