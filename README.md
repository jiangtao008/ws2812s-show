# ws2812sShow

#### 介绍
这是一个ws2812led组成的点阵屏幕，远程显示控制的项目，其中包含上下位机程序。
下位机使用esp32作为控制器，arduino作为代码ide开发，led点阵大小为16*16*三原色，通过wifi连接网络，建立udp服务端接收led显示的数据及控制命令。
上位机为视频播放器及led显示数据编码，使用udp方式传入下位机。

# Adafruit_NeoPixel库基本使用介绍

strip.begin();           // 初始化对象 

//通过下面的方法来设置彩带中各像素的颜色。
strip.setPixelColor(n, red, green, blue); //第一个参数n是彩带中LED的编号，最接近单片机引脚的编号为0；
strip.setPixelColor(n, red, green, blue, white);//最后一个white是可选参数，只适用于带独立正白光的全彩LED，即RGBW型LED。
strip.setPixelColor(n, color);// n是彩带中LED的编号，颜色color是一种32位类型，将红色，绿色和蓝色值合并为一个数字

strip.show(); //要将颜色数据“推送”到彩带，需调用show()方法。

uint32_t color = strip.getPixelColor(11); //该方法返回32位颜色值。

uint16_t n = strip.numPixels(); //查询先前声明的彩带中LED的数量