#include "fire.h"

CFire::CFire()
{

    FireHeight = FireWidth = 0 ;
    m_pWords = NULL ;
    m_pBuffers = NULL ;

    _Play = false ;

    int index = 0;
    for (index=65;index<200;index++)
    {
        mypal[index].peRed   = index+60;
        mypal[index].peGreen = index+30;
        mypal[index].peBlue  = rand()%10;
    }/*
    for (index = 1; index < 35; index++)
    {
        mypal[index].peRed   = index+25;
        mypal[index].peGreen = rand()%10;
        mypal[index].peBlue  = rand()%10;
    }*/
    for (index = 0; index < 40; index++)
    {
        mypal[index].peRed   = 0;
        mypal[index].peGreen = 0;
        mypal[index].peBlue  = 0;
    }
    for (index = 40; index < 45; index++)
    {
        mypal[index].peRed   = index;
        mypal[index].peGreen = index-35;
        mypal[index].peBlue  = rand()%5;
    }
    for (index = 45; index < 65; index++)
    {
        mypal[index].peRed = index+75;
        mypal[index].peGreen = index;
        mypal[index].peBlue = rand()%5;
    }
    for(index = 200; index < 255; index++)
    {
        mypal[index].peRed = index;
        mypal[index].peGreen = index-rand()%25;
        mypal[index].peBlue = rand()%5;
    }
    mypal[0].peRed = 0;
    mypal[0].peGreen = 0;
    mypal[0].peBlue = 0;
}

CFire::~CFire()
{

}

bool CFire::SetFireScreen(int width,int height)
{
    if ( width < 10 )  width = 10;
    if ( height <10 ) height = 10;

    FireHeight = height ;
    FireWidth = width ;

    m_pWords = new unsigned char[height * ( width * 3)];
    m_pBuffers = new unsigned char[height * width];

    if ( m_pWords == NULL || m_pBuffers == NULL )
    {
        return false;
    }
    //CreateBitmap();
    return true;
}

void CFire::CreateBitmap()
{
//    LPBITMAPINFO lpbi;
//    lpbi = (LPBITMAPINFO) new unsigned char[sizeof(BITMAPINFOHEADER)];
//    lpbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
//    lpbi->bmiHeader.biWidth = FireWidth;
//    lpbi->bmiHeader.biHeight = FireHeight;
//    lpbi->bmiHeader.biPlanes = 1;
//    lpbi->bmiHeader.biBitCount = 24;
//    lpbi->bmiHeader.biCompression = BI_RGB;
//    lpbi->bmiHeader.biSizeImage = 0;
//    lpbi->bmiHeader.biXPelsPerMeter = 0;
//    lpbi->bmiHeader.biYPelsPerMeter = 0;
//    lpbi->bmiHeader.biClrUsed = 0;
//    lpbi->bmiHeader.biClrImportant = 0;

//    HBITMAP hBitmap = CreateDIBSection( m_pDC->m_hDC , lpbi, (UINT)DIB_RGB_COLORS, (void **)&m_pWords, NULL, 0 );

//    delete [] (unsigned char *)lpbi;

//    ASSERT(hBitmap != NULL);
//    m_pBitmap.Attach( hBitmap ) ;
}

bool CFire::StartFire()
{
    for(int x = 0; x < FireWidth; x+=(rand()%3))
    {
        if( rand()%2 )
            m_pBuffers[( ( FireHeight - 1 ) * FireWidth ) + x] = 255 ;
        else
            m_pBuffers[( ( FireHeight - 1 ) * FireWidth ) + x] = 0 ;
    }
    _Play = true ;
    return _Play ;
}

bool CFire::DrawToDC()
{
    if ( !_Play )
        return false;

    for( int x = ( FireWidth * FireHeight - 1 ); x >= 0; x--)
    {
        int temp = m_pBuffers[(FireWidth * FireHeight - 1 - x)];
        m_pWords[3*x+2] = mypal[temp].peRed;
        m_pWords[3*x+1] = mypal[temp].peGreen;
        m_pWords[3*x] = mypal[temp].peBlue;
    }
    return StartFire();
}

void CFire::Play()
{
    for( int y = 1; y < FireHeight; y++)
    {
        for( int x = 1; x < FireWidth; x++)
        {
            int xy = ( y * FireWidth ) + x;

            int xyvalue;
            xyvalue = ( m_pBuffers[xy + 1] + m_pBuffers[xy - 1]
            + m_pBuffers[xy + FireWidth] + m_pBuffers[xy - FireWidth]
            + m_pBuffers[xy + FireWidth + 1]
            + m_pBuffers[xy + FireWidth - 1]
            + m_pBuffers[xy - FireWidth + 1]
            + m_pBuffers[xy - FireWidth - 1] ) / 8;

            if ( xyvalue != 0 )
            {
                xyvalue--;
                m_pBuffers[xy - FireWidth] = xyvalue;
            }
        }
    }
}

void CFire::FreeMem()
{
    if ( m_pBuffers != NULL)
    delete m_pBuffers;
    _Play = false;
}


/********************************************************
//dlg类代码
// CFireShowDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CFireShow.h"
#include "CFireShowDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCFireShowDlg dialog

CCFireShowDlg::CCFireShowDlg(CWnd* pParent =NULL)
: CDialog(CCFireShowDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CCFireShowDlg)
    //}}AFX_DATA_INIT
    m_pFire = NULL;
}

void CCFireShowDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CCFireShowDlg)
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCFireShowDlg, CDialog)
//{{AFX_MSG_MAP(CCFireShowDlg)
ON_WM_PAINT()
ON_WM_TIMER()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCFireShowDlg message handlers

bool CCFireShowDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    GetClientRect(rect);

    MemDC = new CDC;
    MemDC->CreateCompatibleDC(NULL);

    m_pFire = new CFire(MemDC);
    if( m_pFire->SetFireScreen(320,103) )
    {
        if( m_pFire->StartFire() )
            SetTimer(1,10,NULL);
    }
    return true;
}

void CCFireShowDlg::OnPaint()
{
    CPaintDC dc(this); // device context for painting

    if ( m_pFire->DrawToDC() )
    {
        MemDC->SelectObject(&(m_pFire->m_pBitmap));
        MemDC->SetBkMode(0);
        CRect rect(0,85,320,100);
        MemDC->SetTextColor(RGB(0,0,0));
        MemDC->DrawText("Gemini",rect,DT_RIGHT);
        dc.BitBlt( 0, 0, 320,103, MemDC, 0, 0, SRCCOPY );
        m_pFire->Play();
    }
    CDialog::OnPaint();
}


void CCFireShowDlg::OnTimer(UINT nIDEvent)
{
    // TODO: Add your message handler code here and/or call default
    InvalidateRect(rect,false);
    CDialog::OnTimer(nIDEvent);
}

bool CCFireShowDlg::DestroyWindow()
{
    // TODO: Add your specialized code here and/or call the base class
    KillTimer(1);
    m_pFire->FreeMem();
    delete(MemDC);
    delete(m_pFire);

    return CDialog::DestroyWindow();
}
*****************************************/
