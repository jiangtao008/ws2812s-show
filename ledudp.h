#ifndef LEDUDP_H
#define LEDUDP_H

#include <QObject>
#include <QUdpSocket>
#include <QHostAddress>
#include <QColor>
class LedUdp : public QObject
{
    Q_OBJECT
public:
    explicit LedUdp(QObject *parent = nullptr);
    ~LedUdp();
    void setRemoto(QHostAddress ip,quint16 port);
    void sendLedData(unsigned char pos, QByteArray data);
    void sendLedAllColor(unsigned char red,unsigned char green,unsigned char blue);
    void sendLedAllColor(QColor color);
    void sendLedMaxLight(unsigned char light);

signals:

public slots:

private:
    QUdpSocket *mUdp;
    QHostAddress mRemoteIP;   //添加远端IP
    quint16 mRemotePort;//添加远端端口号

};

#endif // LEDUDP_H
