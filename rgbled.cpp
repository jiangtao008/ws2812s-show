#include "rgbled.h"

RGBLed::RGBLed(QWidget *parent) :
    QWidget(parent)
    ,mShape(square)
    ,lightFlag(true)
    ,unlightColor(QColor(50,50,50))
{
    setFixedSize(30,30);
    mColor = unlightColor;
}

void RGBLed::setShape(LedShape shape)
{
    mShape = shape;
}

void RGBLed::setShapeToCir()
{
    mShape = circular;
}

void RGBLed::setShapeToSqu()
{
    mShape = square;
}

void RGBLed::setFixedSize(int width,int hight)
{
    this->setMaximumSize(width,hight);
    this->setMinimumSize(width,hight);
    update();
}

void RGBLed::unFixedSize(int miniWidth, int miniHight)
{
    this->setMaximumSize(10000,10000);
    this->setMinimumSize(miniWidth,miniHight);
}

bool RGBLed::isLight()
{
    return lightFlag;
}

void RGBLed::setLight(bool light)
{
    lightFlag = light;
}

void RGBLed::setUnlightColor(QColor color)
{
    unlightColor = color;
}

void RGBLed::setColor(QColor color)
{
    mColor = color;
    update();
}
void RGBLed::setColor(unsigned char red,unsigned char green,unsigned char blue)
{
    mColor = QColor(red,green,blue);
    update();
}


//QByteArray RGBLed::getLedColor()
//{
//    QByteArray buff;
//    buff.append(char(0));
//    buff.append(mColor.red());
//    buff.append(mColor.green());
//    buff.append(mColor.blue());
//    return buff;
//}

QColor RGBLed::getLedColor()
{
    return mColor;
}

void RGBLed::paintEvent(QPaintEvent *paintEvent)
{
    Q_UNUSED(paintEvent);
    QPainter painter(this);

    int edgeWidth = 1;
    int width = this->width();
    int hight = this->height();

    painter.setPen(QPen(QColor(255, 255, 255)));
    QBrush brush(QColor(255,255,255));
    painter.setBrush(brush);
//    //绘制四周边框区域
//    painter.drawRect(QRectF(0, 0, width,edgeWidth));
//    painter.drawRect(QRectF(0, 0, edgeWidth,hight));
//    painter.drawRect(QRectF(0, hight-edgeWidth, width,edgeWidth));
//    painter.drawRect(QRectF(width-edgeWidth, 0, edgeWidth,hight));

    //绘制中心显色区域
    if(lightFlag)
        painter.setBrush(mColor);
    else
        painter.setBrush(unlightColor);

    if(mShape == square)
        painter.drawRect(QRectF(edgeWidth, edgeWidth, width-edgeWidth, hight-edgeWidth));
    else if(mShape == circular)
        painter.drawEllipse(QRectF(edgeWidth, edgeWidth, width-edgeWidth, hight-edgeWidth));

    painter.end();
}

void RGBLed::mouseReleaseEvent(QMouseEvent *event)
{
    lightFlag = !lightFlag;
    update();
}

void RGBLed::HSVtoRGB(unsigned char *r, unsigned char *g, unsigned char *b, int h, int s, int v)
{
    // convert from HSV/HSB to RGB color
    // R,G,B from 0-255, H from 0-260, S,V from 0-100
    // ref http://colorizer.org/
    // The hue (H) of a color refers to which pure color it resembles
    // The saturation (S) of a color describes how white the color is
    // The value (V) of a color, also called its lightness, describes how dark the color is
    int i;
    float RGB_min, RGB_max;
    RGB_max = v*2.55f;
    RGB_min = RGB_max*(100 - s)/ 100.0f;

    i = h / 60;
    int difs = h % 60; // factorial part of h
    float RGB_Adj = (RGB_max - RGB_min)*difs / 60.0f; // RGB adjustment amount by hue

    switch (i) {
    case 0:
        *r = RGB_max;
        *g = RGB_min + RGB_Adj;
        *b = RGB_min;
        break;
    case 1:
        *r = RGB_max - RGB_Adj;
        *g = RGB_max;
        *b = RGB_min;
        break;
    case 2:
        *r = RGB_min;
        *g = RGB_max;
        *b = RGB_min + RGB_Adj;
        break;
    case 3:
        *r = RGB_min;
        *g = RGB_max - RGB_Adj;
        *b = RGB_max;
        break;
    case 4:
        *r = RGB_min + RGB_Adj;
        *g = RGB_min;
        *b = RGB_max;
        break;
    default:  // case 5:
        *r = RGB_max;
        *g = RGB_min;
        *b = RGB_max - RGB_Adj;
        break;
    }
}
