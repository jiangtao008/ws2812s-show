#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTimer>
#include <QLabel>
#include <QMovie>

unsigned char color_model1[768] =
{
    0xbf, 0x00, 0x00, 0xbf, 0x03, 0x00, 0xbf, 0x06, 0x00, 0xbf, 0x0c, 0x00, 0xbf, 0x0f, 0x00, 0xbf, 0x16, 0x00, 0xbf, 0x19, 0x00, 0xbf, 0x1c, 0x00, 0xbf, 0x23, 0x00, 0xbf, 0x26, 0x00, 0xbf, 0x2c, 0x00, 0xbf, 0x2f, 0x00, 0xbf, 0x33, 0x00, 0xbf, 0x39, 0x00, 0xbf, 0x3c, 0x00, 0xbf, 0x42, 0x00,
    0xbf, 0x46, 0x00, 0xbf, 0x49, 0x00, 0xbf, 0x4f, 0x00, 0xbf, 0x52, 0x00, 0xbf, 0x59, 0x00, 0xbf, 0x5c, 0x00, 0xbf, 0x5f, 0x00, 0xbf, 0x66, 0x00, 0xbf, 0x69, 0x00, 0xbf, 0x6f, 0x00, 0xbf, 0x72, 0x00, 0xbf, 0x75, 0x00, 0xbf, 0x7c, 0x00, 0xbf, 0x7f, 0x00, 0xbf, 0x85, 0x00, 0xbf, 0x89, 0x00,
    0xbf, 0x8f, 0x00, 0xbf, 0x92, 0x00, 0xbf, 0x95, 0x00, 0xbf, 0x9c, 0x00, 0xbf, 0x9f, 0x00, 0xbf, 0xa5, 0x00, 0xbf, 0xa8, 0x00, 0xbf, 0xac, 0x00, 0xbf, 0xb2, 0x00, 0xbf, 0xb5, 0x00, 0xbf, 0xbc, 0x00, 0xbf, 0xbf, 0x00, 0xbc, 0xbf, 0x00, 0xb5, 0xbf, 0x00, 0xb2, 0xbf, 0x00, 0xac, 0xbf, 0x00,
    0xa8, 0xbf, 0x00, 0xa5, 0xbf, 0x00, 0x9f, 0xbf, 0x00, 0x9c, 0xbf, 0x00, 0x95, 0xbf, 0x00, 0x92, 0xbf, 0x00, 0x8f, 0xbf, 0x00, 0x89, 0xbf, 0x00, 0x85, 0xbf, 0x00, 0x7f, 0xbf, 0x00, 0x7c, 0xbf, 0x00, 0x79, 0xbf, 0x00, 0x72, 0xbf, 0x00, 0x6f, 0xbf, 0x00, 0x69, 0xbf, 0x00, 0x66, 0xbf, 0x00,
    0x5f, 0xbf, 0x00, 0x5c, 0xbf, 0x00, 0x59, 0xbf, 0x00, 0x52, 0xbf, 0x00, 0x4f, 0xbf, 0x00, 0x49, 0xbf, 0x00, 0x46, 0xbf, 0x00, 0x42, 0xbf, 0x00, 0x3c, 0xbf, 0x00, 0x39, 0xbf, 0x00, 0x33, 0xbf, 0x00, 0x2f, 0xbf, 0x00, 0x2c, 0xbf, 0x00, 0x26, 0xbf, 0x00, 0x23, 0xbf, 0x00, 0x1c, 0xbf, 0x00,
    0x19, 0xbf, 0x00, 0x16, 0xbf, 0x00, 0x0f, 0xbf, 0x00, 0x0c, 0xbf, 0x00, 0x06, 0xbf, 0x00, 0x03, 0xbf, 0x00, 0x00, 0xbf, 0x00, 0x00, 0xbf, 0x06, 0x00, 0xbf, 0x09, 0x00, 0xbf, 0x0f, 0x00, 0xbf, 0x13, 0x00, 0xbf, 0x16, 0x00, 0xbf, 0x1c, 0x00, 0xbf, 0x1f, 0x00, 0xbf, 0x26, 0x00, 0xbf, 0x29,
    0x00, 0xbf, 0x2f, 0x00, 0xbf, 0x33, 0x00, 0xbf, 0x36, 0x00, 0xbf, 0x3c, 0x00, 0xbf, 0x3f, 0x00, 0xbf, 0x46, 0x00, 0xbf, 0x49, 0x00, 0xbf, 0x4c, 0x00, 0xbf, 0x52, 0x00, 0xbf, 0x56, 0x00, 0xbf, 0x5c, 0x00, 0xbf, 0x5f, 0x00, 0xbf, 0x62, 0x00, 0xbf, 0x69, 0x00, 0xbf, 0x6c, 0x00, 0xbf, 0x72,
    0x00, 0xbf, 0x75, 0x00, 0xbf, 0x79, 0x00, 0xbf, 0x7f, 0x00, 0xbf, 0x82, 0x00, 0xbf, 0x89, 0x00, 0xbf, 0x8c, 0x00, 0xbf, 0x8f, 0x00, 0xbf, 0x95, 0x00, 0xbf, 0x99, 0x00, 0xbf, 0x9f, 0x00, 0xbf, 0xa2, 0x00, 0xbf, 0xa5, 0x00, 0xbf, 0xac, 0x00, 0xbf, 0xaf, 0x00, 0xbf, 0xb5, 0x00, 0xbf, 0xb8,
    0x00, 0xbf, 0xbf, 0x00, 0xbc, 0xbf, 0x00, 0xb8, 0xbf, 0x00, 0xb2, 0xbf, 0x00, 0xaf, 0xbf, 0x00, 0xa8, 0xbf, 0x00, 0xa5, 0xbf, 0x00, 0xa2, 0xbf, 0x00, 0x9c, 0xbf, 0x00, 0x99, 0xbf, 0x00, 0x92, 0xbf, 0x00, 0x8f, 0xbf, 0x00, 0x8c, 0xbf, 0x00, 0x85, 0xbf, 0x00, 0x82, 0xbf, 0x00, 0x7c, 0xbf,
    0x00, 0x79, 0xbf, 0x00, 0x75, 0xbf, 0x00, 0x6f, 0xbf, 0x00, 0x6c, 0xbf, 0x00, 0x66, 0xbf, 0x00, 0x62, 0xbf, 0x00, 0x5f, 0xbf, 0x00, 0x59, 0xbf, 0x00, 0x56, 0xbf, 0x00, 0x4f, 0xbf, 0x00, 0x4c, 0xbf, 0x00, 0x49, 0xbf, 0x00, 0x42, 0xbf, 0x00, 0x3f, 0xbf, 0x00, 0x39, 0xbf, 0x00, 0x36, 0xbf,
    0x00, 0x2f, 0xbf, 0x00, 0x2c, 0xbf, 0x00, 0x29, 0xbf, 0x00, 0x23, 0xbf, 0x00, 0x1f, 0xbf, 0x00, 0x19, 0xbf, 0x00, 0x16, 0xbf, 0x00, 0x13, 0xbf, 0x00, 0x0c, 0xbf, 0x00, 0x09, 0xbf, 0x00, 0x03, 0xbf, 0x00, 0x00, 0xbf, 0x03, 0x00, 0xbf, 0x09, 0x00, 0xbf, 0x0c, 0x00, 0xbf, 0x13, 0x00, 0xbf,
    0x16, 0x00, 0xbf, 0x19, 0x00, 0xbf, 0x1f, 0x00, 0xbf, 0x23, 0x00, 0xbf, 0x29, 0x00, 0xbf, 0x2c, 0x00, 0xbf, 0x2f, 0x00, 0xbf, 0x36, 0x00, 0xbf, 0x39, 0x00, 0xbf, 0x3f, 0x00, 0xbf, 0x42, 0x00, 0xbf, 0x46, 0x00, 0xbf, 0x4c, 0x00, 0xbf, 0x4f, 0x00, 0xbf, 0x56, 0x00, 0xbf, 0x59, 0x00, 0xbf,
    0x5f, 0x00, 0xbf, 0x62, 0x00, 0xbf, 0x66, 0x00, 0xbf, 0x6c, 0x00, 0xbf, 0x6f, 0x00, 0xbf, 0x75, 0x00, 0xbf, 0x79, 0x00, 0xbf, 0x7c, 0x00, 0xbf, 0x82, 0x00, 0xbf, 0x85, 0x00, 0xbf, 0x8c, 0x00, 0xbf, 0x8f, 0x00, 0xbf, 0x92, 0x00, 0xbf, 0x99, 0x00, 0xbf, 0x9c, 0x00, 0xbf, 0xa2, 0x00, 0xbf,
    0xa5, 0x00, 0xbf, 0xa8, 0x00, 0xbf, 0xaf, 0x00, 0xbf, 0xb2, 0x00, 0xbf, 0xb8, 0x00, 0xbf, 0xbc, 0x00, 0xbf, 0xbf, 0x00, 0xbf, 0xbf, 0x00, 0xb8, 0xbf, 0x00, 0xb5, 0xbf, 0x00, 0xaf, 0xbf, 0x00, 0xac, 0xbf, 0x00, 0xa8, 0xbf, 0x00, 0xa2, 0xbf, 0x00, 0x9f, 0xbf, 0x00, 0x99, 0xbf, 0x00, 0x95,
    0xbf, 0x00, 0x8f, 0xbf, 0x00, 0x8c, 0xbf, 0x00, 0x89, 0xbf, 0x00, 0x82, 0xbf, 0x00, 0x7f, 0xbf, 0x00, 0x79, 0xbf, 0x00, 0x75, 0xbf, 0x00, 0x72, 0xbf, 0x00, 0x6c, 0xbf, 0x00, 0x69, 0xbf, 0x00, 0x62, 0xbf, 0x00, 0x5f, 0xbf, 0x00, 0x5c, 0xbf, 0x00, 0x56, 0xbf, 0x00, 0x52, 0xbf, 0x00, 0x4c,
    0xbf, 0x00, 0x49, 0xbf, 0x00, 0x46, 0xbf, 0x00, 0x3f, 0xbf, 0x00, 0x3c, 0xbf, 0x00, 0x36, 0xbf, 0x00, 0x33, 0xbf, 0x00, 0x2f, 0xbf, 0x00, 0x29, 0xbf, 0x00, 0x26, 0xbf, 0x00, 0x1f, 0xbf, 0x00, 0x1c, 0xbf, 0x00, 0x19, 0xbf, 0x00, 0x13, 0xbf, 0x00, 0x0f, 0xbf, 0x00, 0x09, 0xbf, 0x00, 0x06
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    ,ledShowModel(1)
{
    ui->setupUi(this);

    //ledUdp.setRemoto(QHostAddress("127.0.0.1"),4569);
    //ledUdp.setRemoto(QHostAddress("192.168.0.114"),4569);

    for(int i = 0;i < 256;i ++)
    {
        RGBLed *led = new RGBLed(this);
        ledList.append(led);
        ui->gridLayout->addWidget(led,i/16,i%16);

        unsigned char r = rand() % 256;
        unsigned char g = rand() % 256;
        unsigned char b = rand() % 256;
        led->setColor(r,g,b);
        //led->setShapeToCir();
        led->setUnlightColor(QColor(0,0,0));
    }

    movie = new QMovie("E:/Code/ledShow/test.gif");
    ui->label_img->setMovie(movie);
    movie->setScaledSize(ui->label_img->size());

    mtime = new QTimer;
    connect(mtime,&QTimer::timeout,this,[=]()
    {
        if(ledShowModel == 1)
            showModel_11(16);
        else if(ledShowModel == 2)
            showModel_2();
        else if(ledShowModel == 3)
            showModel_3();
        else if(ledShowModel == 4)
            showModel_4();
    });
    mtime->setInterval(100);
    ui->lineEdit->setText(QString("%1").arg(mtime->interval()));

    ui->pushButton_start->setEnabled(1);
    ui->pushButton_stop->setEnabled(0);
    int w = 16;
    int h = 16;
    grain.setScreenSize(w,h);
    grain.inint(0,h-1, 5,0, 0,-9.8);
    ui->widget->setFanSpeed(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    ledShowModel =  index + 1;

    if(ledShowModel == 3)
        movie->start();
    else
        movie->stop();
}

void MainWindow::on_pushButton_start_clicked()
{
    mtime->setInterval(ui->lineEdit->text().toInt());
    mtime->start();
    ui->pushButton_start->setEnabled(0);
    ui->pushButton_stop->setEnabled(1);
}
void MainWindow::on_pushButton_stop_clicked()
{
    mtime->stop();
    ui->pushButton_start->setEnabled(1);
    ui->pushButton_stop->setEnabled(0);
}

void MainWindow::showModel_1(int speed,bool vh)      //七彩颜色循环滚动
{
    static int offset = 0;
    offset += speed;
    QByteArray buff;
    if(offset > 360)    offset -= 360;
    for(int i = 0;i < ledList.size();i ++)
    {
        unsigned char r,g,b;
        int h = i*360/ledList.size() + offset;
        if(h >= 360)    h -= 360;
        RGBLed::HSVtoRGB(&r,&g,&b,h,100,75);
        int pos = i;
        if(vh) pos = i%16*16 + i/16;
        ledList.at(i)->setColor(r,g,b);  //纵向向滚动
    }
}
void MainWindow::showModel_11(int speed,bool vh)      //七彩颜色循环滚动
{
    static int offset = 0;
    offset += speed;
    if(offset > 256)    offset -= 256;
    for(int i = 0;i < ledList.size();i ++)
    {
        int ledPos = i%16*16 + i/16;
        int pos1 = (ledPos+offset)*3;
        int pos2 = pos1 +1;
        int pos3 = pos1 +2;
        if(pos1 >= 768) pos1 -= 768;
        if(pos2 >= 768) pos2 -= 768;
        if(pos3 >= 768) pos3 -= 768;
        unsigned char r = color_model1[pos1];
        unsigned char g = color_model1[pos2];
        unsigned char b = color_model1[pos3];
        ledList.at(i)->setColor(r,g,b);  //纵向向滚动
    }
}


void MainWindow::showModel_2()
{
    static float offset = 0;
    offset += 5;
    if(offset >= 360)
        offset -= 360;
    for(int i = 0;i < 16;i ++)
    {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        int h = i*22.5 + offset;
        if(h >= 360)
            h -= 360;
        for(int x = 0;x < 16;x ++)
        {
            RGBLed::HSVtoRGB(&r,&g,&b,h,100,75+x);
            ledList.at(i*16 + x)->setColor(r,g,b);
        }
    }
}


void MainWindow::showModel_3()
{
    QImage img;
    if(movie->isValid())
    {
        QRgb col;
        img = movie->currentImage();
        img = img.scaled(16,16);
        for (int i = 0; i < img.width(); ++i)
        {
            for (int j = 0; j < img.height(); ++j)
            {
                col = img.pixel(i, j);
                int r = qRed(col);
                int g = qGreen(col);
                int b = qBlue(col);
                ledList.at(j*16 + i)->setColor(r,g,b);
            }
        }
    }
}

void MainWindow::showModel_4()
{
    grain.advance();
    qDebug()<<QString("x: %1  y:%2   sX:%3  sY:%4    aX:%5  aY:%6")
                .arg(grain.posX()).arg(grain.posY())
                .arg(grain.speedX()).arg(grain.speedY())
                .arg(grain.accX()).arg(grain.accY());
    for (int i = 0; i < 256; ++i)
        ledList.at(i)->setColor(255,255,255);
    ledList.at(posToI(grain.posX(),grain.posY()))->setColor(0,0,0);
}

void MainWindow::on_lineEdit_editingFinished()
{
    int timeMs = ui->lineEdit->text().toInt();
    mtime->setInterval(timeMs);
}

int MainWindow::posToI(int x, int y)    //x:0-15  y:0-15
{
    return (15-y)*16+x;
}

