#ifndef FIRE_H
#define FIRE_H

#include <QApplication>

//#define NULL nullptr

class CFirePlant
{
public:
unsigned char peRed;
unsigned char peGreen;
unsigned char peBlue;
};


/*********************************

    m_pFire = new CFire(MemDC);
    if( m_pFire->SetFireScreen(320,103) )
    {
        if( m_pFire->StartFire() )
            SetTimer(1,10,NULL);
    }

    if ( m_pFire->DrawToDC() )
    {
        m_pFire->Play();
    }

    m_pFire->FreeMem();
    delete(m_pFire);
 * ******************************/
class CFire
{

public:
    CFire();
    ~CFire();
    bool DrawToDC();
    bool StartFire();
    bool SetFireScreen(int,int);
    void Play();
    void FreeMem();

    CFirePlant mypal[255];
private:
    bool _Play;
    unsigned char *m_pBuffers;
    unsigned char *m_pWords;
    bool flag;
    int FireHeight;
    int FireWidth;
    void CreateBitmap();

};

#endif // FIRE_H
