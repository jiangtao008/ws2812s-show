#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QObject>
#include <QWidget>
#include <QVideoWidget>
#include <QMediaPlayer>

class VideoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VideoWidget(QWidget *parent = nullptr);
    ~VideoWidget();
    void setVideoName(QString name);
    QString getVideoName();  //获取导入的视频名字

    void init();    //播放初始化，初始化一些状态

    bool play();    //启动，无设置视频名情况下失败返回0
    bool pause();
    bool stop();    //关闭，无播放情况下失败返回0
    void setPlaySpeed(double speed);    //设置播放速度

    quint64 getDuration();  //获取视频时长
    bool setPlayPos(quint64 position);
    quint64 getCurrentPlyPos();

    void setVideoBackColor(QColor color);

signals:
    void stateChanged(QMediaPlayer::State); //播放状态改动
    void positionChanged(qint64);   //播放位置改动
    void durationChanged(qint64);   //播放时间改动

private slots:
    void OnStateChanged(QMediaPlayer::State enumState);

private:
    QVideoWidget *mVideoWidget;
    QMediaPlayer *mPlayer;
    QString videoName;
    QMediaPlayer::State mState;

    void resizeEvent(QResizeEvent *event);
};

#endif // VIDEOWIDGET_H
