#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "rgbled.h"
#include "ledudp.h"
#include "grain.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:

    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
    void on_lineEdit_editingFinished();
    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;
    QList<RGBLed *> ledList;
    LedUdp ledUdp;
    int ledShowModel;
    QTimer *mtime;
    QMovie *movie;

    Grain grain;

    void showModel_1(int speed, bool vh = 0);
    void showModel_11(int speed, bool vh = 0);
    void showModel_2();
    void showModel_3();
    void showModel_4();

    int posToI(int x,int y);
};

#endif // MAINWINDOW_H
