#ifndef GRAIN_H
#define GRAIN_H

//#include <QObject>
#include <math.h>
class Grain
{
public:
    explicit Grain();
    void inint(float posX,float posY,float speedX,float speedY,float accX,float accY);  //设置模拟对象初始化状态
    void setScreenSize(int width,int hight);        //设置模拟范围，宽和高
    void setGrainSize(int w,int h);
    void setCoefFriction(float coef);
    void setCoefBump(float coef);
    void setPos(float x,float y);
    void setSpeed(float x,float y);
    void setAcc(float x,float y);

    //获取粒子参数
    float posX()  { return mPosX;    }
    float posY()  { return mPosY;    }
    float speedX(){ return mSpeedX;  }
    float speedY(){ return mSpeedY;  }
    float accX()  { return mAccX;    }
    float accY()  { return mAccY;    }
    float setCoefFriction() {   return mCoefFriction;    }
    float setCoefBump()     {   return mCoefBump;        }

    void advance(); //驱动模拟运行函数

private:
    float mPosX;    //x位置(m)  范围：0 -  mWidth
    float mPosY;    //y位置(m)  范围：0 - mHight
    float mSpeedX;  //x轴速度(m/s)
    float mSpeedY;  //y轴速度(m/s)
    float mAccX;    //x轴加速度(m/s2)
    float mAccY;    //y轴加速度(m/s2)

    float mCoefFriction;    //速度摩擦系数，每次推进，速度 = 速度` * 摩擦系数(0-1)
    float mCoefBump;        //撞击摩擦系数，每次推进，速度 = 速度` * 撞击系数(0-1)

    int mWidth,mHight;      //屏幕范围，宽和高(m)
    int mGrainW,mGrainH;    //粒子大小
};

#endif // GRAIN_H
