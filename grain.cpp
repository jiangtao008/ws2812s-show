#include "grain.h"

Grain::Grain()
{
    mCoefFriction = 0.99;
    mCoefBump = 0.95;
    mAccX = 0;
    mAccY = -9.8;   //重力加速度
}
void Grain::inint(float posX, float posY, float speedX, float speedY, float accX, float accY)
{
    setPos(posX,posY);
    setSpeed(speedX,speedY);
    setAcc(accX,accY);
}
void Grain::setScreenSize(int width, int hight)    //设置模拟范围
{
    mWidth = width;
    mHight = hight;
}
void Grain::setGrainSize(int w, int h)
{
    mGrainW = w;
    mGrainH = h;
}
void Grain::setPos(float x, float y)    //设置当前位置参数
{
    mPosX = x;
    mPosY = y;
}
void Grain::setSpeed(float x, float y)    //设置速度参数
{
    mSpeedX = x;
    mSpeedY = y;
}
void Grain::setAcc(float x, float y)    //设置加速度参数
{
    mAccX = x;
    mAccY = y;
}
void Grain::setCoefFriction(float coef)    //设置速度摩擦系数
{
    mCoefFriction = coef;
}
void Grain::setCoefBump(float coef)    //设置撞击摩擦系数
{
    mCoefBump = coef;
}

/**************************
 (0,h)**...**(w,h)
      **...**
   ↑    ...
      **...**
 (0,0)**...**(w,0)
         →
**************************/
void Grain::advance()
{
    //参数
    float time = 0.1;  //s
    float aX = mAccX;
    float aY = mAccY;
    int runWidth = mWidth - mGrainW;
    int runHight = mHight - mGrainH;

    //考虑到墙壁
    if(((mPosX >= runWidth-1) && (mSpeedX > 0))||((mPosX <= 0) && (mSpeedX < 0)))    //左右
    {
        aX = 0.0;                       //撞墙时，加速度为0（和墙壁支撑力抵消）
        mSpeedX = abs(mSpeedX) * mCoefBump;  //撞墙后速度不仅相反，还要有速度损耗
        if(mPosX >= runWidth-1)
            mSpeedX = -mSpeedX;             //反弹，速度相反
    }
    else
        mSpeedX *= mCoefFriction;    //速度损耗（摩檫力，和速度成正比）
    mSpeedX += aX * time;   //更新速度
    //if((abs(mSpeedX) < 0.1) && (aX < 0.01)) mSpeedX = 0;//速度过小后，直接停止(静摩擦)
    mPosX   += mSpeedX * time;  //更新位置
    if(mPosX >= mWidth-1)   mPosX = mWidth-1;    //添加位置限制
    if(mPosX <= 0)          mPosX = 0;

    if(((mPosY >= runWidth-1) && (mSpeedY > 0)) || ((mPosY <= 0) && (mSpeedY < 0)))    //上下
    {
        aY = 0.0;
        mSpeedY = abs(mSpeedY) * mCoefBump;
        if(mPosY >= runWidth-1)
            mSpeedY = -mSpeedY;
    }
    else
        mSpeedY *= mCoefFriction;
    mSpeedY += aY * time;
    //if((abs(mSpeedY) <= 0.1) && (aY < 0.01)) mSpeedY = 0 ;
    mPosY   += mSpeedY * time;
    if(mPosY >= mWidth-1)   mPosY = mWidth-1;
    if(mPosY <= 0)          mPosY = 0;




}
