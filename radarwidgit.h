#ifndef RADARWIDGIT_H
#define RADARWIDGIT_H

#include <QWidget>
#include <QTimer>

class RadarWidgit : public QWidget
{
    Q_OBJECT

public:
    explicit RadarWidgit(QWidget *parent = nullptr);

    void setBackColor(QColor color);
    void setDrawColor(QColor color);
    void setFanColor(QColor color);
    void setFanSpeed(int speed);

protected:
    void paintEvent(QPaintEvent *event);    //绘制事件
    void resizeEvent(QResizeEvent *event);  //大小重置事件
    void timerEvent(QTimerEvent *event);    //定时器事件

private:
    QRect         m_drawArea;      //绘制区域
    int           m_pieRotate;     //扇形旋转区域
    int           m_timerId;       //定时器ID
    int           m_pointTimerId;  //变更点定时器
    int           m_nSpeed;        //扇形旋转速度
    QList<QPoint> m_points;        //绘制点
    //QList<int>    m_pointsAlapha;  //绘制点颜色alapha值
    QColor m_backColor;            //背景颜色
    QColor m_drawColor;            //绘制颜色
    QColor m_fanColor;             //扇形颜色

    QTimer fanTime;

};



#endif // RADARWIDGIT_H
