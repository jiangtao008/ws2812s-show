#include "ledudp.h"

LedUdp::LedUdp(QObject *parent) : QObject(parent)
{
    mUdp = new QUdpSocket();
    mRemoteIP = QHostAddress("192.168.0.114");
    mRemotePort = 4569;
}

LedUdp::~LedUdp()
{
    delete mUdp;
}

void LedUdp::setRemoto(QHostAddress ip, quint16 port)
{
    mRemoteIP = ip;
    mRemotePort = port;
}

void LedUdp::sendLedData(unsigned char pos, QByteArray data)
{
    if(data.size() < 256)
        data.append(QByteArray(256-data.size(),0x00));
    QByteArray buff;
    buff.append(0x69);
    buff.append(0xaa);
    buff.append(pos);
    buff.append(data);
    mUdp->writeDatagram(buff,mRemoteIP,mRemotePort);
}

void LedUdp::sendLedAllColor(unsigned char red, unsigned char green, unsigned char blue)
{
    QByteArray buff;
    buff.append(0x69);
    buff.append(0xaa);
    buff.append(0x04);
    buff.append((char)red);
    buff.append((char)green);
    buff.append((char)blue);
    mUdp->writeDatagram(buff,mRemoteIP,mRemotePort);
}

void LedUdp::sendLedAllColor( QColor color)
{
    sendLedAllColor(color.red(),color.green(),color.blue());
}

void LedUdp::sendLedMaxLight(unsigned char light)
{
    QByteArray buff;
    buff.append(0x69);
    buff.append(0xaa);
    buff.append(0x05);
    buff.append(light);
    mUdp->writeDatagram(buff,mRemoteIP,mRemotePort);
}
